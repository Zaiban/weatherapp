import React from 'react';
import ReactDOM from 'react-dom';
import moment from 'moment';
import _ from 'lodash';

import { USE_GEOLOCATION } from './config';

const baseURL = process.env.ENDPOINT;

const getLocationFromBrowser = async () => {
  if (navigator && navigator.geolocation) {
    return new Promise((resolve, reject) => {
      const parseCoords = (position) => {
        try {
          const query = `lat=${position.coords.latitude}&lon=${position.coords.longitude}`;
          resolve(query);
        } catch (err) {
          reject(err);
        }
      };
      navigator.geolocation.getCurrentPosition(parseCoords, reject);
    });
  }
  return undefined;
};

const getWeatherFromApi = async ({ location }) => {
  try {
    const query = location ? `?${location}` : '';
    const response = await fetch(`${baseURL}/weather${query}`);
    return response.json();
  } catch (error) {
    console.error(error);
  }

  return {};
};

class Weather extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      formattedWeatherList: [],
      weatherLocation: '',
    };
  }

  async componentWillMount() {
    const location = USE_GEOLOCATION ? await getLocationFromBrowser() : undefined;

    const { weatherList, weatherLocation } = await getWeatherFromApi({ location });

    const formattedWeatherList = weatherList.map((item) => {
      const icon = item.weather[0].icon.slice(0, -1);
      const time = moment.unix(item.dt);
      return { icon, time };
    });

    this.setState({ formattedWeatherList, weatherLocation });
  }

  render() {
    const { formattedWeatherList, weatherLocation } = this.state;

    return (
      <div>
        <h2>Showing weather: {weatherLocation}</h2>
        { formattedWeatherList.map(item => (
          <div key={_.uniqueId()} className="array-item">
            <div>{item.time.format('LLL')}</div>
            <div>{item.time.fromNow()}</div>
            <img alt="icon" src={`/img/${item.icon}.svg`} className="icon" />
          </div>
          ))}
      </div>
    );
  }
}

ReactDOM.render(
  <Weather />,
  document.getElementById('app')
);
