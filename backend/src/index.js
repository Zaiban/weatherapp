// const debug = require('debug')('weathermap');

const Koa = require('koa');
const router = require('koa-router')();
const fetch = require('node-fetch');
const cors = require('kcors');
const Url = require('url');

const appId = process.env.APPID || '';
const mapURI = process.env.MAP_ENDPOINT || 'http://api.openweathermap.org/data/2.5';
const targetCity = process.env.TARGET_CITY || 'Helsinki,fi';

const port = process.env.PORT || 9000;

const app = new Koa();

app.use(cors());

const fetchWeather = async ({ coordinates, }) => {
  const locationQuery = coordinates || `q=${targetCity}`;
  const response = await fetch(`${mapURI}/forecast?appid=${appId}&${locationQuery}`);

  return response ? response.json() : {};
};

router.get('/api/weather', async ctx => {
  const url = Url.parse(ctx.url, true);

  const coordinates = url.query && url.query.lat && url.query.lon
    ? `lat=${url.query.lat}&lon=${url.query.lon}` : undefined;

  const weatherData = await fetchWeather({ coordinates, });

  ctx.type = 'application/json; charset=utf-8';
  // ctx.body = weatherData.list ? weatherData.list[0].weather[0] : {};
  const weatherList = weatherData.list ? weatherData.list.slice(0, 10) : [];
  const weatherLocation = coordinates ? 'local' : targetCity;
  ctx.body = { weatherList, weatherLocation, };
});

app.use(router.routes());
app.use(router.allowedMethods());

app.listen(port);

console.log(`App listening on port ${port}`);
